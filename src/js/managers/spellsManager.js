class SpellsConnectionManager{

    constructor(){
        this.ping();
    }

    ENDPOINT = (path) => `http://localhost:8000/v1/${path}`;

    HTTP_STATUS_LOG = (response) => `${response.status}:${response.statusText}`;

    getQueryParamsNames(){
        return {
            NAME: "nombre",
            CLASS: "clase",
            SPECIALIZATION: "familia",
            NEW: "nuevo"
        };
    }

    ping(){
        const EXPECTED_RESPONSE = 'pong';
        console.log('Testing connection... Ping!');

        fetch(this.ENDPOINT('ping'), {
          method: 'GET'
        })
        .then(response => {
            if (response.status==200) {
                return response.json();
            }else{
                console.warn(this.HTTP_STATUS_LOG(response));
                return [];
            }
        })
        .then(data => {
            if (data.DEBUG === EXPECTED_RESPONSE) {
                console.log('Pong!');
            } else {
                console.error(`Error. Unexpected response from manager: ${data.ERROR}`);
            }
        })
        .catch(error => console.error(error));

    }
    
    async findAllSpellsViewData(queryParams){
        return fetch(this.ENDPOINT(`hechizos/${queryParams}`), {
          method: 'GET'
        })
        .then(response => {
            if (response.status==200) {
                return response.json();
            }else{
                console.warn(this.HTTP_STATUS_LOG(response));
                return [];
            }
        })
        .catch(error => console.error(error));
    }

    // TO MIGRATE
    // findAllSpellsData(endFunction=this.printData){
    //     $.post(this.MANAGER_PATH,
    //         {
    //             operacion:'getAllHechizos()'
    //         },
    //         endFunction
    //     );
    // }

    // findSpellDataByName(name, endFunction=this.printData){
    //     $.post(
    //         this.MANAGER_PATH,
    //         {
    //             operacion:'findHechizoByNombre()',
    //             nombre:name
    //         },
    //         endFunction
    //     );    
    // }
    
    // findAllSpellTypes(endFunction=this.printData){
    //     $.post(this.MANAGER_PATH,
    //         {
    //             operacion:'getAllTiposHechizos()'
    //         },
    //         endFunction
    //     );
    // }
    
    // findAllSpellSchools(endFunction=this.printData){
    //     $.post(this.MANAGER_PATH,
    //         {
    //             operacion:'getAllTiposEscuelas()'
    //         },
    //         endFunction
    //     );
    // }
    
    // findAllSpellTargets(endFunction=this.printData){
    //     $.post(this.MANAGER_PATH,
    //         {
    //             operacion:'getAllTiposObjetivos()'
    //         },
    //         endFunction
    //     );
    // }

    // findSpellsViewDataByFilter(filter, endFunction=this.printData){
    //     $.post(this.MANAGER_PATH,
    //         {
    //             operacion:'findHechizosFromVistaByFilter()',
    //             filtro:filter
    //         },
    //         endFunction
    //     );
    // }

    // insertSpell(newSpell, endFunction=this.printData){
    //     $.post(
    //         this.MANAGER_PATH,
    //         {
    //             operacion:'insertaHechizo()',
    //             hechizo:newSpell
    //         },
    //         endFunction
    //     );
    // }

    // updateSpell(newSpell, endFunction=this.printData){
    //     $.post(
    //         this.MANAGER_PATH,
    //         {
    //             operacion:'modificaHechizo()',
    //             hechizo:newSpell
    //         },
    //         endFunction
    //     );
    // }
}