class ClassesConnectionManager{

    constructor(){
        this.ping();
    }

    ENDPOINT = (path) => `http://localhost:8000/v1/${path}`

    async ping(){
        const EXPECTED_RESPONSE = 'pong';
        console.log('Testing connection... Ping!');

        fetch(this.ENDPOINT('ping'), {
          method: 'GET'
        })
        .then(response => response.json())
        .then(data => {
            if (data.DEBUG === EXPECTED_RESPONSE) {
                console.log('Pong!');
            } else {
                console.error(`Error. Unexpected response from manager: ${data.ERROR}`);
            }
        })
        .catch(error => console.error(error));

    }

    async getAllClasses(){
        return fetch(this.ENDPOINT('hechizos/familias/?group'), {
                  method: 'GET'
                })
        .then(response => response.json())
        .catch(error => console.error(error));
    }

    //TO MIGRATE
    // async findClassByName(name, endFunction=this.printData){
    //     $.post(this.MANAGER_PATH,
    //         {   
    //             operacion:'findClaseByNombre()',
    //             nombre:name
    //         },
    //         endFunction
    //     );
    // }

    // async insertClass(newClass, endFunction=this.printData){
    //     $.post(this.MANAGER_PATH,
    //         {
    //             operacion:'insertaClase()',
    //             clase:newClass
    //         }, 
    //         endFunction
    //     );
    // }

    // async updateClass(newClass, endFunction=this.printData){
	// 	$.post(this.MANAGER_PATH,
    //         {
    //             operacion:'modificaClase()',
    //             clase:newClass
    //         }
    //         ,endFunction
	// 	);
    // }

    // async printData(data){ console.log(data) }
}