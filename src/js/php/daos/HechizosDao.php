<?
include 'vos/Hechizo.php';
include 'vos/Tipo.php';
include 'vos/Filtro.php';
/**
 * 
 */
class HechizosDao
{
	// **************** SELECT ********************

	// ---------------- TABLA -------------------- 

	static function getLastId(){
		include 'conecta.php';
		$sql="SELECT ID FROM ".$T_CLASES." ORDER BY ID DESC LIMIT 1";
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all();
		return $datos[0][0];
	}

	static function getAllTiposHechizos(){
		include 'conecta.php';
		return self::getAllTipos($T_TIPOS_HECHIZOS);
	}

	static function getAllTiposEscuelas(){
		include 'conecta.php';
		return self::getAllTipos($T_TIPOS_ESCUELAS);
	}

	public static function getAllTiposObjetivos(){
		include 'conecta.php';
		return self::getAllTipos($T_TIPOS_OBJETIVOS);
	}

	private static function getAllTipos($tabla){
		include 'conecta.php';
		$sql="SELECT * FROM ".$tabla;
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		$tipos = Tipo::parseTipoArrayFromDB($datos);
    	return $tipos;	
	}	

	public static function findHechizosByFilter($filtro){
		include 'conecta.php';
		$sql="SELECT * FROM ".$T_HECHIZOS." ".$filtro." ".$filtro->getLimit();
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		$num_rows=count($datos);
		return ($num_rows!=0)?Hechizo::parseHechizoArrayFromDB($datos):null;
	}	

	public static function getAllHechizos(){
		include 'conecta.php';
		$sql="SELECT * FROM ".$T_HECHIZOS." ORDER BY NOMBRE ASC";
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		$num_rows=count($datos);
		return ($num_rows!=0)?Hechizo::parseHechizoArrayFromDB($datos):null;
	}
	// ---------------- VISTA -------------------- 

	public static function getAllHechizosFromVista(){
		include 'conecta.php';
		$sql="SELECT * FROM ".$V_HECHIZOS." ORDER BY CLASE, TALENTO, LVL ASC";
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		$num_rows=count($datos);
		return ($num_rows!=0)?Hechizo::parseHechizoArrayFromVista($datos):null;
	}

	public static function findHechizosFromVistaByFilter($filtro){
		include 'conecta.php';
		$sql="SELECT * FROM ".$V_HECHIZOS." ".$filtro." ORDER BY CLASE, TALENTO, LVL ASC ".$filtro->getLimit();
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		$num_rows=count($datos);
		return ($num_rows!=0)?Hechizo::parseHechizoArrayFromVista($datos,$filtro->getNuevo()):null;
	}

	// **************** QUERYS ********************

	public static function insertaHechizo($hechizo){
		include 'conecta.php';
		$sql = "INSERT INTO ".$T_HECHIZOS." (ID_CLASE, ID_TALENTO, NOMBRE, COSTE, REQUISITO, LVL, ID_OBJETIVO, ID_TIPO, NUM_TIPO, ALCANCE, EFECTO, FECHA_MODIFICACION, ID_ESCUELA, DURACION, LEGENDARIO) VALUES (".
		$hechizo->clase.", ". 
		$hechizo->talento.", ". 
		"'".$hechizo->nombre."', ". 
		$hechizo->coste.", ".
		"'".$hechizo->requisito."', ".
		$hechizo->lvl.", ".
		$hechizo->objetivo.", ".
		$hechizo->tipo.", ".
		$hechizo->numTipo.", ".
		$hechizo->alcance.", ".
		"'".$hechizo->efecto."', ".
		"current_timestamp(), ".
		$hechizo->escuela.", ". 
		"'".$hechizo->duracion."', ". 
		$hechizo->legendario.");";
		$mysqli->query($sql);
		return "OK;;Hechizo insertado correctamente";
	}

	public static function modificaHechizo($hechizo){
		include 'conecta.php';
		$sql = "UPDATE ".$T_HECHIZOS." SET ".
		"ID_CLASE = ".$hechizo->clase.", ".
		"ID_TALENTO = ".$hechizo->talento.", ".
		"ID_ESCUELA = ".$hechizo->escuela.", ".
		"NOMBRE = '".$hechizo->nombre."', ".
		"DURACION = '".$hechizo->duracion."', ".
		"COSTE = ".$hechizo->coste.", ".
		"REQUISITO = '".$hechizo->requisito."', ".
		"LVL = ".$hechizo->lvl.", ".
		"ID_OBJETIVO = ".$hechizo->objetivo.", ".
		"ID_TIPO = ".$hechizo->tipo.", ".
		"NUM_TIPO = ".$hechizo->numTipo.", ".
		"ALCANCE = ".$hechizo->alcance.", ".
		"EFECTO = '".$hechizo->efecto."', ".
		"FECHA_MODIFICACION = CURRENT_TIMESTAMP, ".
		"LEGENDARIO = ".$hechizo->legendario." WHERE ID = ".$hechizo->id; 
		$mysqli->query($sql);
		return "OK;;Hechizo modificado correctamente";
	}
}
?>