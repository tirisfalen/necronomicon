<?
include 'vos/Clase.php';

/**
 * 
 */
class ClasesDao
{
	// **************** SELECT ********************

	// ---------------- TABLA -------------------- 

	static function getLastId(){
		include 'conecta.php';
		$sql="SELECT ID FROM ".$T_CLASES." ORDER BY ID DESC LIMIT 1";
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all();
		return $datos[0][0];
	}	

	public function findClaseById($id){
		// TODO
	}

	public static function findClaseByNombre($nombre){
		include 'conecta.php';
		$sql="SELECT * FROM ".$T_CLASES." WHERE `NOMBRE`=\"".$nombre."\"";
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		$num_rows=count($datos);
		return ($num_rows!=0)?Clase::parseClaseFromDB($datos[0]):null;
	}	

	public static function getAllClases(){
		include 'conecta.php';
		$sql="SELECT * FROM ".$T_CLASES." ORDER BY NOMBRE";
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		$num_rows=count($datos);
		$clases = array('');
    	for ($i=0; $i <$num_rows ; $i++) { 
    		$clases[$i] = Clase::parseClaseFromDB($datos[$i]);
    	}
    	return $clases;
	}

	// **************** QUERYS ********************

	public static function insertaClase($clase){
		include 'conecta.php';
		$sql = "INSERT INTO ".$T_CLASES." (COLOR, NOMBRE) VALUES ('".$clase->color."', '".$clase->nombre."');";
		$mysqli->query($sql);
		return true;
	}

	public static function updateClase($clase){
		include 'conecta.php';
		$sql = "UPDATE ".$T_CLASES." SET COLOR ='".$clase->color."', NOMBRE ='".$clase->nombre."' WHERE ID = ".$clase->id;
		$mysqli->query($sql);
		return true;
	}

	public static function deleteClase($id){
		// TODO
	}
}
?>