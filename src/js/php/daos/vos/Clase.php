<?
include 'Talento.php';

/**
 * VO de una clase
 */
class Clase
{
	//VARIABLES
	public $id;
	public $nombre;
	public $color;
	public $talentos;//arrObjetos

	public function __construct($id, $nombre, $color, $talentos)
	{
		$this->id=$id;
		$this->nombre=$nombre;
		$this->color=$color;
		$this->talentos=$talentos;
	}

	public static function parseClaseFromDB($arr){
		return new Clase($arr['ID'],$arr['NOMBRE'],$arr['COLOR'],null);
	}

    //Estan en minuscula porque son las variables de esta clase
    public static function getInstanceOf($arr){
        return new Clase($arr['id'],$arr['nombre'],$arr['color'],null);
    }

    public static function getInstanceOfArray($arr){
        $clases;
        for ($i=0; $i < count($arr); $i++) { 
            $clases[$i]=self::getInstanceOf($arr[$i]);
        }
        return $clases;
    }

    public function hasTalentos(){
    	return !($this->talentos==null);
    }

    public function talentosListToDelete(){
    	$talentosToDelete = array('');
    	$contador=0;
    	for ($i=0; $i < count($this->talentos); $i++) { 
    		if ($this->talentos[$i]->toDelete()){
				$talentosToDelete[$contador] = $this->talentos[$i];
				$contador++;
    		}
    	}
    	return ($contador==0)?null:$talentosToDelete;
    }

    public function talentosListToInsert(){
    	$talentosToInsert = array('');
    	$contador=0;
    	 for ($i=0; $i < count($this->talentos); $i++) { 
    		if ($this->talentos[$i]->toInsert()){
				$talentosToInsert[$contador] = $this->talentos[$i];
				$contador++;
    		}
    	}
    	return ($contador==0)?null:$talentosToInsert;
    }

    public function talentosListToUpdate(){
    	$talentosToUpdate = array('');
    	$contador=0;
    	for ($i=0; $i < count($this->talentos); $i++) { 
    		if (!($this->talentos[$i]->toInsert() || $this->talentos[$i]->toDelete())){
				$talentosToUpdate[$contador] = $this->talentos[$i];
				$contador++;
    		}
    	}
    	return ($contador==0)?null:$talentosToUpdate;
    }

    public function addTalento($talento){
    	$this->talentos[] = $talento;
    }

	public function __toString(){
        if (isset($this->talentos)) {
    		$string = $this->nombre." {";
    		for ($i=0; $i <count($this->talentos) ; $i++) { 
        		$string = $string." ".$this->talentos[$i].",";
        	}
            return substr($string, 0, -1)." }";
        }

        return $this->nombre;
    }
}
?>