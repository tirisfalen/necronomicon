<?

/**
 * VO de un hechizo
 */
class Hechizo
{
	//VARIABLES
	public $id;
	public $clase;
	public $talento;
	public $nombre;
	public $coste;
	public $requisito;
	public $lvl;
	public $objetivo;
	public $tipo;
	public $numTipo;
	public $alcance;
	public $efecto;
	public $fechaModificacion;
	public $legendario;
	public $escuela;
	public $duracion;

	public $DIAS_NUEVO=14;
	public $nuevo;

	public function __construct($id, $clase, $talento, $nombre, $coste, $requisito, 
								$lvl, $objetivo, $tipo, $numTipo, $alcance, $efecto, 
								$fechaModificacion, $legendario, $escuela, $duracion)
	{
		$this->id=$id;
		$this->clase=$clase;
		$this->talento=$talento;
		$this->nombre=$nombre;
		$this->coste=$coste;
		$this->requisito=$requisito;
		$this->lvl=$lvl;
		$this->objetivo=$objetivo;
		$this->tipo=$tipo;
		$this->numTipo=$numTipo;
		$this->alcance=$alcance;
		$this->efecto=$efecto;
		$this->fechaModificacion=$fechaModificacion;
		$this->legendario=$legendario;
		$this->escuela=$escuela;
		$this->duracion=$duracion;

		$this->nuevo=self::isNuevo();
	}

	public static function parseHechizoFromDB($arr){
		return new Hechizo($arr['ID'],$arr['ID_CLASE'],$arr['ID_TALENTO'],$arr['NOMBRE'],
						$arr['COSTE'],$arr['REQUISITO'],$arr['LVL'],$arr['ID_OBJETIVO'],
						$arr['ID_TIPO'],$arr['NUM_TIPO'],$arr['ALCANCE'],$arr['EFECTO'],
						$arr['FECHA_MODIFICACION'],$arr['LEGENDARIO']
						,$arr['ID_ESCUELA'],$arr['DURACION']);
	}

	public static function parseHechizoFromVista($arr){
		return new Hechizo($arr['ID'],$arr['CLASE'],$arr['TALENTO'],$arr['NOMBRE'],
						$arr['COSTE'],$arr['REQUISITO'],$arr['LVL'],$arr['OBJETIVO'],
						$arr['TIPO'],$arr['NUM_TIPO'],$arr['ALCANCE'],$arr['EFECTO'],
						$arr['FECHA_MODIFICACION'],$arr['LEGENDARIO']
						,$arr['ESCUELA'],$arr['DURACION']);
	}
	
	//Estan en minuscula porque son las variables de esta clase
	public static function getInstanceOf($arr){
		return new Hechizo($arr['id'],$arr['clase'],$arr['talento'],$arr['nombre'],
						$arr['coste'],$arr['requisito'],$arr['lvl'],$arr['objetivo'],
						$arr['tipo'],$arr['numTipo'],$arr['alcance'],$arr['efecto'],
						$arr['fechaModificacion'],$arr['legendario']
						,$arr['escuela'],$arr['duracion']);
	}

	function isNuevo(){
		if($this->fechaModificacion==null) return false;

		$FORMATO_DIAS='%a';

		$hoy=date_create();
		$fechaModificacion=date_create($this->fechaModificacion);
		$diffDias = date_diff($hoy,$fechaModificacion)->format($FORMATO_DIAS);
		return ($diffDias<=$this->DIAS_NUEVO);
	}

	public static function parseHechizoArrayFromVista($arr,$compruebaNuevo=false){
    	$hechizos;
    	$contador = 0;
    	for ($i=0; $i <count($arr) ; $i++) { 
    		$hechizo = self::parseHechizoFromVista($arr[$i]);
			if(!$compruebaNuevo){
				$hechizos[$contador] = $hechizo;
    			$contador++;
			}
    		else if($hechizo->nuevo) {
				$hechizos[$contador] = $hechizo;
				$contador++;
    		}
    	}
    	return $hechizos;
	}

	public static function parseHechizoArrayFromDB($arr){
    	$hechizos;
    	for ($i=0; $i <count($arr) ; $i++) { 
    		$hechizos[$i] = self::parseHechizoFromDB($arr[$i]);
    	}
    	return $hechizos;
	}

	public static function getInstanceOfArray($arr){
    	$hechizos;
    	for ($i=0; $i <count($arr) ; $i++) { 
    		$hechizos[$i] = self::getInstanceOf($arr[$i]);
    	}
    	return $hechizos;
	}

	public function __toString(){
		return $this->nombre;
    }
}
?>