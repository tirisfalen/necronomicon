const DROPDOWN_MENU_IMG_FORMAT = '.ico';
const CLASSES_IMAGE_PATH = 'src/img/clases/';
const CLASS_IMAGE_PATH = (id) => `${CLASSES_IMAGE_PATH}${id}${DROPDOWN_MENU_IMG_FORMAT}`

const CLASSES_MANAGER = new ClassesConnectionManager();
const SPELLS_MANAGER = new SpellsConnectionManager();
const SPELL_FILTER_ATTR_NAMES = SPELLS_MANAGER.getQueryParamsNames();

const SEARCH_BAR_ID = "searchinput";
const REFRESH_BTN_ID = "refresh-btn";
const DEFAULT_MENU_ITEM_ID = "Clases";
const DROPDOWN_MENU_BUTTON_ID = "dropdownButton";

const ENTER_KEY = 13;

$( document ).ready(function(){

	setupRefreshButton();
	setupSearchBar();
	
	let urlParams = getUrlParams();
	loadSpellContainerData(urlParams);
	setSearchBarValue(urlParams.get(SPELL_FILTER_ATTR_NAMES.NAME));
	loadSpellClassesData(urlParams.get(SPELL_FILTER_ATTR_NAMES.CLASS));
});

async function setupRefreshButton(){

	document.getElementById(REFRESH_BTN_ID).addEventListener('click', function() {
	    
	    setSearchBarValue();
	    setDropdownMenuValue();

	    let path = window.location.pathname;
	    window.history.replaceState(null, null, path);

	    loadSpellContainerData();
	});
}

async function setupSearchBar(){
	const searchBar = document.getElementById(SEARCH_BAR_ID);

	searchBar.addEventListener('keypress', function(key) {
	    if (key.which !== ENTER_KEY) {
	        return;
	    }

	    findAndLoadSpellsBy(SPELL_FILTER_ATTR_NAMES.NAME, this.value);
	});
}

async function setSearchBarValue(spellName){
	if (spellName===undefined || spellName===null) {
		spellName = ''
	}

	const searchBar = document.getElementById(SEARCH_BAR_ID);
	searchBar.value = spellName;

	const keypress = new Event('keypress');
    keypress.which = ENTER_KEY;
    searchBar.dispatchEvent(keypress);
}

async function loadSpellContainerData(urlParams){

	let queryParams = urlParamsToString(urlParams);

	try {
		SPELLS_MANAGER.findAllSpellsViewData(queryParams)
						.then(spellList => {
							console.info(spellList);
							buildSpellCardContainer(spellList);
							setupSpellcardFilterListener();
						});
	} catch (error) {
		console.error('Error al obtener datos:', error);
	}


}

async function loadSpellClassesData(className){
	try {
		CLASSES_MANAGER.getAllClasses()
		.then(spellClassList => {
			console.info(spellClassList);
			buildDropdownMenuOptions(spellClassList);	
			setDropdownMenuValue(className);
			updateAllSpellsStyleByClassList(spellClassList);
		})
	} catch (error) {
		console.error('Error al obtener datos:', error);
	}
}

function buildDropdownMenuOptions(classes){
	if (classes===null){
		return;
	}

	for (i in classes){
		buildDropdownMenuOption(classes[i]);
	}

	setupDropdownMenuOptionListener();
}

function setDropdownMenuValue(className){
	
	const MENU_BUTTON = document.getElementById(DROPDOWN_MENU_BUTTON_ID);

	if (className===undefined || className===null) {
		className = DEFAULT_MENU_ITEM_ID;
	}

	const SELECTED_ITEM = document.getElementById(className);
	className = className.toLowerCase();
	className = className.charAt(0).toUpperCase() + className.slice(1);

	rebuildDropdownMenuAfterSelect(SELECTED_ITEM, MENU_BUTTON.lastElementChild, MENU_BUTTON);
}

function buildDropdownMenuOption(spellClass){
	var dropdownMenu = document.querySelector('.dropdown-menu');
	var liElement = document.createElement('li');
	liElement.id = spellClass.NOMBRE;

	var aElement = document.createElement('a');
	aElement.href = '#';

	var imgElement = document.createElement('img');
	imgElement.src = CLASS_IMAGE_PATH(spellClass.ID);

	var spanElement = document.createElement('span');
	spanElement.textContent = spellClass.NOMBRE;

	aElement.appendChild(imgElement);
	aElement.appendChild(spanElement);
	liElement.appendChild(aElement);

	dropdownMenu.appendChild(liElement);
}

function setupDropdownMenuOptionListener(){
    const MENU_BUTTON = document.getElementById(DROPDOWN_MENU_BUTTON_ID);

	document.querySelectorAll("#dropdown li").forEach(function(item) {
	  item.addEventListener("click", function() {

	    const SELECTED_ITEM = this;
	    const LAST_SELECTED_ITEM = MENU_BUTTON.lastElementChild;

	    if (SELECTED_ITEM === LAST_SELECTED_ITEM) {
			return;
	    }

	    rebuildDropdownMenuAfterSelect(SELECTED_ITEM, LAST_SELECTED_ITEM, MENU_BUTTON);

	    if (SELECTED_ITEM.id === DEFAULT_MENU_ITEM_ID) {
		    findAndLoadSpellsBy(SPELL_FILTER_ATTR_NAMES.CLASS, "");
	    	return;
	    }
	    	
    	findAndLoadSpellsBy(SPELL_FILTER_ATTR_NAMES.CLASS, SELECTED_ITEM.id);
	  });
	});
}

function rebuildDropdownMenuAfterSelect(menuItem, lastSelectedItem, menuButton){
	const dropdownMenu = menuItem.parentNode;
	let insertLast = true;

	lastSelectedItem.remove();

	let menuOptionsArray = Array.prototype.slice.call(dropdownMenu.children);
	for(i in menuOptionsArray){
		
		let option = menuOptionsArray[i];
		if (lastSelectedItem.id<option.id) {
			dropdownMenu.insertBefore(lastSelectedItem, option);
			insertLast = false;
			break;
		}
	}

	if (insertLast) {
		dropdownMenu.insertBefore(lastSelectedItem,dropdownMenu.children.lastElementChild);
	}

	menuButton.appendChild(menuItem);
}

function buildSpellCardContainer(spellList){
	removeSpellCards();

	if (spellList===undefined || spellList===null) {
		updateResultNumber(0);
		return;
	}

	updateResultNumber(spellList.length);
	loadSpellCardList(spellList);
}

function removeSpellCards(){
	$('#spells-all').children().remove();
}

function updateResultNumber(number){
	$("#resultados p span").text(number);
}

function loadSpellCardList(spellList){
	for(i in spellList){
		loadSpellCard(spellList[i]);
	}
}

function loadSpellCard(spell){
	let title = spell.NOMBRE;
	title = title.replace(/\s+/g, '-').replace(/'/g, 'A').toLowerCase();
	let claseTalento = `${spell.CLASE}_${spell.FAMILIA}`;
	let newSpellClass = spell.NUEVO?' newSpell':'';

	let requisitoListItem = ''
	if (spell.requisito!=="") {
		requisitoListItem = `<li class="components"><span class="bold">Requisito</span><p>${spell.REQUISITO}</p></li>`;
	}

	$('#spells-all').append(`
	    <li data-id="${title}" class="card${newSpellClass}">
	        <ul class="${claseTalento}">
	            <a class="mana" href="#"><span>${spell.COSTE}</span></a>
	            <li class="title">${spell.NOMBRE}</li>
	            <li class="type">
	                ${spell.TIPO} ${spell.ESCUELA} de Nivel ${spell.LVL}
	                <p>Objetivo ${spell.OBJETIVO}</p>
	            </li>
	            <li class="casting_time"><span class="bold">Turnos</span><p>${spell.NUM_TIPO} turnos</p></li>
	            <li class="range"><span class="bold">Rango</span><p>${spell.ALCANCE}m</p></li>
	            <li class="duration"><span class="bold">Duración</span><p>${spell.DURACION}</p></li>
	            ${requisitoListItem}
	            <li class="descr"><p class="card-text">${spell.EFECTO}</p></li>
	            <div class="espec"><span class="bold">${spell.FAMILIA}</span></div>
	        </ul>
	    </li>
	`);

}

function setupSpellcardFilterListener(){
	$('.espec span').click(function(){
		findAndLoadSpellsBy(SPELL_FILTER_ATTR_NAMES.SPECIALIZATION, $(this).text());
	});

	$('.title').click(function(){
		let spell = getSpellFromCard($(this).parent().parent().attr('data-id'));
		let readableSpell = spellToUserReadableFormat(spell)
		copySpellToClipboard(readableSpell);

		console.log(readableSpell);
	})
}

function findAndLoadSpellsBy(attrName, value){
	let urlParams = getUrlParams();
	setUrlParamsValue(urlParams, attrName, value);
	updateLocation(urlParams);
	loadSpellContainerData(urlParams);
}

function getSpellFromCard(dataID){
	let spellSelector = "[data-id="+dataID+"]";

	console.debug(`getSpellFromCard() -> ${spellSelector}`);

	spell = {
		nombre: 				$(spellSelector+" .title").text(),
		coste:  				$(spellSelector+" .mana span").text(),
		tipo:  					$(spellSelector+" .type").text(),
		tiempo_lanzamiento:  	$(spellSelector+" .casting_time p").text(),
		rango:  				$(spellSelector+" .range p").text(),
		duracion:  				$(spellSelector+" .duration p").text(),
		objetivo:  				$(spellSelector+" .type p").text(),
		requisito:  			$(spellSelector+" .components p").text(),
		efecto:  				$(spellSelector+" .descr p").text()
	};

	console.info(spell);

	return spell;
}

function copySpellToClipboard(spell){
	const EMPTY="";
	const CLIPBOARD_SELECTOR = '#clipboard';

	$(CLIPBOARD_SELECTOR).val(spell);
	$(CLIPBOARD_SELECTOR).select();
	document.execCommand("copy");
	$(CLIPBOARD_SELECTOR).val(EMPTY);
}

function spellToUserReadableFormat(spell){
	const BREAK="\r\n";
	const TAB = "\t";
	
	let component = (spell.requisito !== "") ? `${TAB}Requisito: ${spell.requisito}${BREAK}` : "";
	let formattedSpell = 
	  `-${spell.nombre} (${spell.coste} PM)${BREAK}` +
	  `${TAB}${spell.tipo}${BREAK}` +
	  `${TAB}Turnos lanzamiento: ${spell.tiempo_lanzamiento}${BREAK}` +
	  `${TAB}Rango: ${spell.rango}${BREAK}` +
	  `${TAB}Duracion: ${spell.duracion}${BREAK}` +
	  `${component}${BREAK}` +
	  `${TAB}${spell.efecto}`;
	
	return formattedSpell;
}