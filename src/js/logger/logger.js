//TODO: pendiente refactorizar formato output de los parametros para que sea general y mas facil de leer
const VOID = "[void]";

class MyLogger{

    constructor(discartedFunctionsToLog=new Array(), ignoreDiscartedFunctionToLog=true, ignoreInvalidFunctionsToLog=true){

        const DEFAULT_DISCARTED_FUNCTIONS_TO_LOG = ['parseFunctionString','printFunctionLog',
                                                            'getValueListFromArguments',
                                                            'parseToString',
                                                            'setTimeout','getComputedStyle','$'
                                                            ,'requestAnimationFrame','jQuery'];

        this.discartedFunctionsToLog = new Array(...DEFAULT_DISCARTED_FUNCTIONS_TO_LOG);
        this.discartedFunctionsToLog.push(...discartedFunctionsToLog);
        this.ignoreDiscartedFunctionToLog = ignoreDiscartedFunctionToLog;
        this.ignoreInvalidFunctionsToLog = ignoreInvalidFunctionsToLog;
    }

    __init() {
        //TODO: Estas funciones estan pendientes de mover, para mantener la estrucutra a POO
        //deberian ser funciones del objeto MyLogger pero usar el this dentro de la nueva funcion
        //linea (80) crea conflicto con el scope de la funcion. Hasta que encuentre fix para esto queda asi.
        function getValueListFromArguments(args){
            let valueList = new Array();
            for(let i in args){
                valueList.push(args[i]);
            }
            return valueList;
        }

        function printFunctionLog(fnString, argValues, result){
            let parsedLog = parseFunctionString(fnString,argValues,result);
            if(parsedLog===undefined){
                return;
            }
            console.log(parsedLog);
        }
    
        function parseFunctionString(fnStr, argValues, result) {
            const matches = fnStr.match(/^function\s+(\w+)\(([^)]*)\)/);
    
            if (!matches) {
                if(!this.ignoreInvalidFunctionsToLog){
                    console.log('Invalid function to log: '+fnStr);
                }
                return;
            }
            const ARG_NAME_SEPARATOR = ",";
    
            const funcName = matches[1];
            let argStr;

            if(matches[2].includes(ARG_NAME_SEPARATOR)){
                let argNames = matches[2].split(ARG_NAME_SEPARATOR);
                let args = {};
                for (let i = 0; i < argNames.length; i++) {
                    args[argNames[i]] = parseToString(argValues[i]);
                }
                argStr = Object.entries(args).map(([name, value]) => `[${name}: ${value}]`).join(' ');
            }else{
                //TODO: Aqui puede pasar que tenga un solo argumento, hay que validar eso antes de setearlo a vacio
                //Fix express
                if(matches[2]!=""){
                    let args = {};
                    args[matches[2]] = parseToString(argValues[0]);
                    argStr = Object.entries(args).map(([name, value]) => `[${name}: ${value}]`).join(' ');
                }else{
                    argStr = VOID;
                }
            }

            result = parseToString(result);
    
            return `${funcName} ${argStr} => ${result}`;
        }

        const FUNCTION_TYPE = 'function'
        
        for (let name in window) {
            let fn = window[name];
            
            if (typeof fn !== FUNCTION_TYPE) { continue; }
    
            if (this.discartedFunctionsToLog.includes(name)){ 
                if(!this.ignoreDiscartedFunctionToLog){
                    console.log("Discarted function to log: "+name)
                }
                continue; 
            }

            window[name] = (function(name, fn) {
                return function() {
                    const ARG_VALUES = getValueListFromArguments(arguments);
                    const NOT_CALCULATED_RESULT = 'RUN';
                    
                    printFunctionLog(fn.toString(),ARG_VALUES,NOT_CALCULATED_RESULT);

                    const RESULT = fn.apply(this, arguments);
                    printFunctionLog(fn.toString(),ARG_VALUES,RESULT);
                    return RESULT;
                }
            })(name, fn);
        }
    }
}

function parseToString(element){
    if(element instanceof Element){
        return "'"+element.outerHTML+"'";
    }
    if(element instanceof URLSearchParams){
        return "'"+element.toString()+"'";
    }
    if(element instanceof Object){
        return "'"+Object.entries(element).map(([key, value]) => `{${parseToString(key)}: ${parseToString(value)}}`).join(' ')+"'";
    }
    if(element===null){
        return "'null'";
    }
    if(element===undefined){
        return VOID;
    }
    return "'"+element.toString()+"'";
}