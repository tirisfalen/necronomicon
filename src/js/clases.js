// Inputs
const INPUT_ID_SELECTOR = "#class_id";
const INPUT_NAME_SELECTOR = "#class_name";
const INPUT_TXT_COLOR_SELECTOR = "#txt_color";
const INPUT_COLOR_SELECTOR = "#nombre";
const INPUT_ALL_SPEC_ID_SELECTOR = "#talentos .fila .id";
const INPUT_ALL_SPEC_NAME_SELECTOR = "#talentos .fila .texto_nombre";
const INPUT_ALL_SPEC_COLOR_SELECTOR = "#talentos .fila .hex_color";
// Labels
const LBL_COLOR_SELECTOR = "#lbl_nombre";
// Buttons
const BTN_INSERT_SELECTOR = "#agregar";
const BTN_UPDATE_SELECTOR = "#modificar";
const BTN_ADD_SPEC_SELECTOR = "#talentos button";
// Misc
const SPEC_CONTAINER_SELECTOR = "#talentos";
const SPEC_ROWS_SELECTOR = "#talentos .fila";
// Visualizer
const VISUALIZER_LAST_ROW_SELECTOR = '#visualizer table tr:last';
const VISUALIZER_CLASS_NAME_ROW_SELECTOR = '#bg_nombre';
const VISUALIZER_CLASS_NAME_SELECTOR = '#bg_nombre #texto_nombre';

const DEFAULT_COLOR_PICKER_COLOR = "#ffffff";
const CLASS_NAME_PLACE_HOLDER = "Clase";
const CLASS_SPEC_PLACE_HOLDER = "Especialización";
const DEFAULT_ID = '0';

const CLASSES_CONNECTION_MANAGER = new ClassesConnectionManager("../src/php/ClasesManager.php");

$( document ).ready(function(){
	const LOGGER = new MyLogger();
	LOGGER.__init();
	setupButtonEvents();
	setupInputEvents();

	let params = getUrlParams();
	if (validateUrlParams(params)) {
		CLASSES_CONNECTION_MANAGER.findClassByName(params.get('nombre'),parseAndSetClassData);
	}else{
		setupDefaultInputValues();
	}
});  

function parseAndSetClassData(classData){
	try {
		setClass(JSON.parse(classData));
	} catch (error) {
		console.log(classData);
		console.log(error);
	}
}

function setupButtonEvents(){
	//TODO: refactorizar
	$(BTN_ADD_SPEC_SELECTOR).click(function() {
		const index = $(SPEC_ROWS_SELECTOR).length;
		const newInputRowSelector = `#bg_talento_${index}`
		const newInputSpecRow=buildSpecInputRow(index);
		const newVisualizerSpecRow = buildSpecVisualizerRow(index);
		
		$(SPEC_CONTAINER_SELECTOR).append(newInputSpecRow);
		$(VISUALIZER_LAST_ROW_SELECTOR).before(newVisualizerSpecRow);
		$(newInputRowSelector).show('fast');

		//TODO: se podria parametrizar setupInputEvents, pero eso involucra muchos argumentos,
		//buscar la forma de con menos selectores, o trabajar con un selector 
		//mas simple, como solo el contenedor o algo asi.
		let inputTxtColorSelector = "input[name=hex_talento_"+index+"]";
		let inputTxtNameSelector = "input[name=talento_"+index+"]";
		let inputColorSelector = "#talento_"+index;
		let visualizerNameSelector = '#texto_talento_'+index+'';
		let elementsToChangeColor = ['#lbl_talento_'+index, newInputRowSelector];
		
		syncInputValueWithElementText(inputTxtNameSelector, visualizerNameSelector)
		syncColorPickerListener(inputTxtColorSelector, inputColorSelector);
		syncColorPickerChangeWithElementsColor(inputColorSelector, elementsToChangeColor);
		$(inputColorSelector).val(DEFAULT_COLOR_PICKER_COLOR).change();
	});

	$(BTN_INSERT_SELECTOR).click(function(){
		let roleClass = getClass();
		if(validateClass(roleClass, false)){
			CLASSES_CONNECTION_MANAGER.insertClass(roleClass);
		}else{
			console.error('Invalid class: '+roleClass.nombre);
		}
	})

	$(BTN_UPDATE_SELECTOR).click(function(){
		let roleClass = getClass();
		if(validateClass(roleClass,true)){
			CLASSES_CONNECTION_MANAGER.updateClass(roleClass);
		}else{
			console.error('Invalid class: '+roleClass.nombre);
		}
	})
}

function buildSpecInputRow(index){
	return `
		<div class="fila" id="fila_${index}">
			<input class="id" type="hidden" name="id_talento_${index}" value="0">
			<input class="texto_nombre" type="text" name="talento_${index}" placeholder="${CLASS_SPEC_PLACE_HOLDER}">
			<input class="hex_color" type="text" name="hex_talento_${index}" placeholder="#Color">
			<label id="lbl_talento_${index}" for="talento_${index}" class="circulo-picker"></label>
			<input id="talento_${index}" type="color" name="color" hidden="">
		</div>
	`
}

function buildSpecVisualizerRow(index){
	return `
		<tr class="fila" style="display: none;" id="bg_talento_${index}">
			<td id="texto_talento_${index}" colspan="3">
				${CLASS_SPEC_PLACE_HOLDER}
			</td>
		</tr>
  	`;
}

function setupInputEvents(){
	syncInputValueWithElementText(INPUT_NAME_SELECTOR, VISUALIZER_CLASS_NAME_SELECTOR);
	syncColorPickerListener(INPUT_TXT_COLOR_SELECTOR,INPUT_COLOR_SELECTOR);
	syncColorPickerChangeWithElementsColor(INPUT_COLOR_SELECTOR, 
											[LBL_COLOR_SELECTOR, VISUALIZER_CLASS_NAME_ROW_SELECTOR])
}

function syncInputValueWithElementText(inputSelector, targetSelector){
	$(inputSelector).keyup(
		keyupEvent => $(targetSelector).text(getInputValue(keyupEvent.target))
	);
}

function syncColorPickerChangeWithElementsColor(colorPickerSelector, elementsSelector){
	$(colorPickerSelector).change(
		changeEvent => changeFontAndBackgroundColorOf(elementsSelector, 
													  changeEvent.target.value)
	);
}

function syncColorPickerListener(textInput, colorInput){
	$(textInput).keyup(
		keyupEvent => syncColorWith(colorInput, 
									keyupEvent.target.value)
	);
	$(colorInput).change(
		changeEvent => syncColorWith(textInput, 
									changeEvent.target.value)
	);
}

function changeFontAndBackgroundColorOf(targetsSelector, color){
	for(i in targetsSelector){
		let targetSelector = targetsSelector[i];
		$(targetSelector).css('background-color',color);
		$(targetSelector).css('color',pickTextColorBasedOnBg(color));
	}
}

function syncColorWith(targetSelector, hexColor){
	let syncedValue = $(targetSelector).val();
	
	if(syncedValue!==undefined && syncedValue===hexColor){
		return;
	}

	hexColor = validateHexColor(hexColor);
	if(hexColor===undefined){
		return;
	}
	
	$(targetSelector).val(hexColor).change();
}

function validateHexColor(hexColor){
	const VALID_LENGTH = 7;
	const MIN_LENGTH = 6;
	const HASH = '#';

	if(hexColor.length<MIN_LENGTH){
		return;
	}

	if(!hexColor.includes(HASH)){
		hexColor = HASH+hexColor;
	}

	if(hexColor.length!=VALID_LENGTH){
		return;
	}

	return hexColor.toUpperCase();
}

function getInputValue(input){
	return (input.value===undefined || input.value==="") ? input.placeholder:input.value;
}

function getInputsValue(inputs){
	let stringArr = new Array();
	inputs.forEach(function(value,key){
		stringArr.push(getInputValue(value));
	});
	return stringArr;
}

function setClass(roleClass){
	$(INPUT_ID_SELECTOR).val(roleClass.id);
	$(INPUT_NAME_SELECTOR).val(roleClass.nombre).keyup();
	$(INPUT_COLOR_SELECTOR).val(roleClass.color).change();
	setSpecializations(roleClass.talentos);
}

function setSpecializations(talentos){
	talentos.forEach(function(value,key){
		$(BTN_ADD_SPEC_SELECTOR).click();
		$(`input[name=talento_${key}]`).val(value.nombre).keyup();
		$(`input[name=id_talento_${key}]`).val(value.id).keyup();
		$(`#talento_${key}`).val(value.color).change();
	})
}

function getClass(){
	return {
		id: getInputValue(document.querySelector(INPUT_ID_SELECTOR)),
		nombre: getInputValue(document.querySelector(INPUT_NAME_SELECTOR)),
		color: getInputValue(document.querySelector(INPUT_TXT_COLOR_SELECTOR)),
		idsTalentos: getInputsValue(document.querySelectorAll(INPUT_ALL_SPEC_ID_SELECTOR)),
		nombreTalentos: getInputsValue(document.querySelectorAll(INPUT_ALL_SPEC_NAME_SELECTOR)),
		colorTalentos: getInputsValue(document.querySelectorAll(INPUT_ALL_SPEC_COLOR_SELECTOR))
	};
}

function validateClass(roleClass, toUpdate){

	if(toUpdate && roleClass.id===DEFAULT_ID){
		console.error('Invalid class identifier: '+DEFAULT_ID);
		return false;
	}
	
	for(i in roleClass.idsTalentos){
		if(roleClass.idsTalentos[i]===DEFAULT_ID){
			console.error('Invalid specialization identifier: '+DEFAULT_ID);
			return false;
		}
	}

	if(roleClass.nombre===CLASS_NAME_PLACE_HOLDER){
		console.error('Invalid class name: '+CLASS_NAME_PLACE_HOLDER);
		return false;
	}

	let validatedColor = validateHexColor(roleClass.color);
	if(validatedColor===undefined){
		console.error('Invalid hex color: '+roleClass.color)
		return false;
	}
	roleClass.color = roleClass.color.replaceAll("#","");
	
	for(i in roleClass.nombreTalentos){
		if(roleClass.nombreTalentos[i]===CLASS_SPEC_PLACE_HOLDER){
			console.error('Invalid specialization name: '+CLASS_SPEC_PLACE_HOLDER);
			return false;
		}
	}
	
	for(i in roleClass.colorTalentos){
		validatedColor = validateHexColor(roleClass.colorTalentos[i]);
		if(validatedColor===undefined){
			console.error('Invalid hex color: '+roleClass.colorTalentos[i])
			return false;
		}
		validatedColor.replaceAll("#","");
	}
	return true;
}

function setupDefaultInputValues(){
	$(INPUT_COLOR_SELECTOR).val(DEFAULT_COLOR_PICKER_COLOR).change();
}