function hexToRgb(hexColor){
	hexColor = (hexColor.charAt(0) === '#') ? hexColor.substring(1, 7) : hexColor;
	let aRgbHex = hexColor.match(/.{1,2}/g);
	let aRgb = [
	    parseInt(aRgbHex[0], 16),
	    parseInt(aRgbHex[1], 16),
	    parseInt(aRgbHex[2], 16)
	];
	return aRgb;
}


function pickTextColorBasedOnBg(bgColor) {
  let aRgb = hexToRgb(bgColor);
  let r = aRgb[0];
  let g = aRgb[1];
  let b = aRgb[2];
  const brightness = Math.round(((parseInt(r) * 299) +
          			 (parseInt(g) * 587) +
          			 (parseInt(b) * 114)) / 1000);
  return (brightness > 125)?"#000000":"#F3EEE8";
}



function updateAllSpellsStyleByClassList(spellClassList){
	for(let spellClass of spellClassList){
		updateSpellStyleByClass(spellClass);
	}
}

function updateSpellStyleByClass(spellClass){
	let specs = spellClass.FAMILIAS;
	for(let i in specs){

		let spec = specs[i];
		let spellSelector = `.${spellClass.NOMBRE}_${spec.NOMBRE}`;
		let specTextSelector = `${spellSelector} .espec span`;
		let backgroundStyle = getSpellBackgroundStyle(spellClass, spec);
		let specColor = pickTextColorBasedOnBg(spec.COLOR);

		$(specTextSelector).css("color",specColor);

		let sheet = window.document.styleSheets[0];
		sheet.insertRule(`${spellSelector} { background: ${backgroundStyle}; }`, sheet.cssRules.length);
		sheet.insertRule(`${specTextSelector} { color: ${specColor}; }`, sheet.cssRules.length);
	}
}

function getSpellBackgroundStyle(spellClass, spec){
	const GRADIENT_DEGREE = 180
	const FIRST_COLOR_PORCENTAGE = 25
	const SECOND_COLOR_PORCENTAGE = 85
	let rgbClassColor = hexToRgb(spellClass.COLOR); 
	let rgbSpecColor = hexToRgb(spec.COLOR); 

	let style = `linear-gradient(
		${GRADIENT_DEGREE}deg, 
		rgb(${rgbClassColor[0]}, ${rgbClassColor[1]}, ${rgbClassColor[2]}) 
		0%, 
		rgba(255, 255, 255, 0) 
		${FIRST_COLOR_PORCENTAGE}%, 
		rgba(255, 255, 255, 0) 
		${SECOND_COLOR_PORCENTAGE}%, 
		rgb(${rgbSpecColor[0]}, ${rgbSpecColor[1]}, ${rgbSpecColor[2]})
		100%
	)`;
	return style;
}