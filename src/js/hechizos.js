const CLASSES_CONNECTION_MANAGER = new ClassesConnectionManager("../src/php/ClasesManager.php");
const SPELLS_CONNECTION_MANAGER = new SpellsConnectionManager("../src/php/HechizosManager.php");

const VALOR_DEFECTO_SEL_CLASES={id:'0', nombre:'Clases', color:'transparent'};
const VALOR_DEFECTO_SEL_TALENTOS={id:'0', nombre:'Especialización', color:'transparent'};
const VALOR_DEFECTO_SEL_OBJETIVOS={id:'0', nombre:'Objetivo'};
const VALOR_DEFECTO_SEL_TIPOS_HECHIZOS={id:'0', nombre:'Tipo'};
const VALOR_DEFECTO_SEL_TIPOS_ESCUELAS={id:'0', nombre:'Escuelas'};

var loadedData = {
	classes : false,
	targets : false,
	spellSchools : false,
	spellTypes : false
}

$( document ).ready(function(){
	const LOGGER = new MyLogger();
	LOGGER.__init();
	
	muestraLoader();
	
	CLASSES_CONNECTION_MANAGER.getAllClasses(parseAndBuildClassSelect);	
	SPELLS_CONNECTION_MANAGER.findAllSpellTypes(parseAndBuildSpellTypeSelect);
	SPELLS_CONNECTION_MANAGER.findAllSpellSchools(parseAndBuildSpellSchoolSelect);
	SPELLS_CONNECTION_MANAGER.findAllSpellTargets(parseAndBuildSpellTargetSelect);
	SPELLS_CONNECTION_MANAGER.findAllSpellsData(parseAndBuildSpellList);

	setupInputListeners();
	setupButtons();
})

function finishLoadContent(){
	console.log('Loading data...'+JSON.stringify(loadedData))

	if(!loadedData.targets){
		return false;
	}
	
	if(!loadedData.classes){
		return false;
	}
	
	if(!loadedData.spellSchools){
		return false;
	}

	if(!loadedData.spellTypes){
		return false;
	}

	let params = getUrlParams();
	if (params!==null) {
		loadSpellData(params.get('nombre'));
	}
	ocultaLoader();
}

function parseAndBuildClassSelect(classesData){
	let roleClassList = JSON.parse(classesData);
	try {
		let roleClassList = JSON.parse(classesData);
		loadedData.classes = true;
	} catch (error) {
		console.error('Cannot parse class data: '+classesData);
		notificacion('ERROR','Cannot load classes data',true);
	}
	updateAllSpellsStyleByClassList(roleClassList)
	roleClassList.unshift(VALOR_DEFECTO_SEL_CLASES);
	buildClassSelect(roleClassList);
	finishLoadContent();
}

function parseAndBuildSpellList(spellsData){
	try {
		setSpellList(JSON.parse(spellsData));
	} catch (error) {
		console.error('Cannot parse spell data: '+spellsData);
	}
}

// TODO: Refactorizar para que el try no sea tan amplio cuando se mire el evento todoCargado
function parseAndBuildSpellTypeSelect(spellTypeData){
	try {
		let spellTypes = JSON.parse(spellTypeData);
		spellTypes.unshift(VALOR_DEFECTO_SEL_TIPOS_HECHIZOS);
		buildSelectOptions('select[name=tipo]',spellTypes);
		loadedData.spellTypes = true;						
		finishLoadContent();
	} catch (error) {
		console.error('Cannot parse type data: '+spellTypeData);
	}
}

// TODO: Refactorizar para que el try no sea tan amplio cuando se mire el evento todoCargado
function parseAndBuildSpellSchoolSelect(schoolTypeData){
	try {
		let spellSchools = JSON.parse(schoolTypeData);
		spellSchools.unshift(VALOR_DEFECTO_SEL_TIPOS_ESCUELAS);
		buildSelectOptions('select[name=escuela]',spellSchools);
		loadedData.spellSchools = true;						
		finishLoadContent();
	} catch (error) {
		console.error('Cannot parse spell school data: '+schoolTypeData);
	}	
}

// TODO: Refactorizar para que el try no sea tan amplio cuando se mire el evento todoCargado
function parseAndBuildSpellTargetSelect(targetTypeData){
	try {
		let targetTypes = JSON.parse(targetTypeData);
		targetTypes.unshift(VALOR_DEFECTO_SEL_OBJETIVOS);
		buildSelectOptions('select[name=objetivo]',targetTypes);
		loadedData.targets = true;
		finishLoadContent();
	} catch (error) {
		console.error('Cannot parse spell target data: '+targetTypeData);
	}			
}

// TODO: Refactorizar evitando el sistema de construir el selector con el target,
//  mirar como se hace ahora en clases.js. Comprobar que se pueda re usar updateElementText.
function cambiaTexto(){
	let texto = ($(this).val()==="")?$(this).attr('placeholder'):$(this).val();
	$('#texto_'+$(this).attr('name')).text(texto);
}


function setSpellList(hechizos){
	const dataListSelector = "#nombre";
	const inputNameSelector = "#listaNombres";
	const spellNameSelector = '#texto_nombre';

	hechizos.forEach(function(item){
		let option = new Option(item.nombre, item.nombre);
		option.setAttribute('data-value',JSON.stringify(item));
		$(dataListSelector).append(option);
	});

	$(inputNameSelector).keyup(function(){
		$(spellNameSelector).text(this.value);
	});

	$(inputNameSelector).change(function(){
		let value = $(this).val();
		if(value===undefined || value===""){
			return;
		}

		hechizos.forEach(function(hechizo){
			if(hechizo.nombre===value){
				setHechizo(hechizo);
				return;
			}
		})
	});
}

function buildClassSelect(roleClassList){
	buildSelectOptions('#selClase',roleClassList);
	$('#selClase').change(changeEvent => buildClassSpecSelect(changeEvent.target.selectedOptions[0]));
}

function buildClassSpecSelect(selectedClassOption){	
	let roleClass;
	try {
		roleClass = JSON.parse(selectedClassOption.dataset.value);
	} catch (error) {
		console.error('Cannot parse '+selectedClassOption.dataset.value)
	}

	let classSpecs = roleClass.talentos;
	classSpecs.unshift(VALOR_DEFECTO_SEL_TALENTOS);
	buildSelectOptions("#selTalento", classSpecs);

	$('#selTalento').change(changeEvent => updateSpecTextAndColor(changeEvent.target.selectedOptions[0]));
}

function updateElementText(selector, newText){
	if (newText===undefined){
		return;
	}
	$(selector).text(newText);
}

function updateSpecTextAndColor(selectedSpecOption){
	let selectedClassOption = document.getElementById('selClase').selectedOptions[0];
	let spec;
	let spellClass;
	try {
		spec = JSON.parse(selectedSpecOption.dataset.value);
		spellClass = JSON.parse(selectedClassOption.dataset.value);
	} catch (error) {
		console.error('Cannot parse specialization: '+selectedSpecOption+' and class: '+spellClass);
		return;
	}
	let specTextSelector = "#texto_talento";
	updateElementText(specTextSelector, spec.nombre);
	document.querySelector('.card ul').className = spellClass.nombre+"_"+spec.nombre;
}

function buildSelectOptions(selectSelector, valueList){
	$(selectSelector+" option").remove();
	$(selectSelector).prop("disabled",true);

	if(valueList===undefined){
		return;
	}

	for(let item of valueList){
		let option = new Option(item.nombre, item.id);
		option.setAttribute('data-value',JSON.stringify(item));
		$(selectSelector).append(option);

		if(item.color!==undefined){
			optionSelector = selectSelector+' option[value='+item.id+']';
			$(optionSelector).css("background-color",item.color);
			$(optionSelector).css("color",pickTextColorBasedOnBg(item.color));
		}
	}

	$(selectSelector).prop("disabled",false);
}

function getSpell(){
	return {
		id: $("#formulario input[name=id]").val(),
		nombre: $("#formulario input[name=nombre]").val(),
		coste: $("#formulario input[name=coste]").val(),
		nivel: $("#formulario input[name=level]").val(),
		tipo: $("#formulario select[name=tipo]").val(),
		numTipo: $("#formulario input[name=num_tipo]").val(),
		alcance: $("#formulario input[name=alcance]").val(),
		objetivo: $("#formulario select[name=objetivo]").val(),
		clase: $("#selClase").val(),
		talento: $("#selTalento").val(),
		escuela: $("#formulario select[name=escuela]").val(),
		duracion: $("#formulario input[name=duracion]").val(),
		requisito: $("#formulario textarea[name=requisito]").val(),
		legendario: 0,
		efecto: $("#formulario textarea[name=efecto]").val()
	};
}

function validateSpell(spell){
	if (spell.idEscuela===null) {
		notificacion("ERROR;;El campo idEscuela no puede estar vacio ");
		return false;
	}
	if (spell.nombre==="" || spell.nombre===undefined) {
		notificacion("ERROR;;El campo nombre no puede estar vacio ", true);
		return false;
	}
	if (spell.coste==="" || spell.coste===undefined) {
		notificacion("ERROR;;El campo coste no puede estar vacio ");
		return false;
	}
	if (spell.nivel==="" || spell.nivel===undefined) {
		notificacion("ERROR;;El campo nivel no puede estar vacio ");
		return false;
	}
	if (spell.numTipo==="" || spell.numTipo===undefined) {
		notificacion("ERROR;;El campo numTipo no puede estar vacio ");
		return false;
	}
	if (spell.alcance==="" || spell.alcance===undefined) {
		notificacion("ERROR;;El campo alcance no puede estar vacio ");
		return false;
	}
	if (spell.efecto==="" || spell.efecto===undefined) {
		notificacion("ERROR;;El campo efecto no puede estar vacio ");
		return false;
	}
	if (spell.duracion==="" || spell.duracion===undefined) {
		notificacion("ERROR;;El campo duracion no puede estar vacio ");
		return false;
	}
	if (spell.tipo==="0") {
		notificacion("ERROR;;El campo tipo no puede estar vacio ");
		return false;
	}
	if (spell.clase==="0") {
		notificacion("ERROR;;El campo clase no puede estar vacio ");
		return false;
	}
	if (spell.escuela==="0") {
		notificacion("ERROR;;El campo escuela no puede estar vacio ");
		return false;
	}
	if (spell.talento==="0") {
		notificacion("ERROR;;El campo talento no puede estar vacio ");
		return false;
	}
	if (spell.objetivo==="0") {
		notificacion("ERROR;;El campo objetivo no puede estar vacio ");
		return false;
	}
	return true;
}

function copiaEnlaceCompartirHechizo(nombreHechizo){
	let url = window.location.href;
	url=url.replace(/\?.+$/, '');
	url=url.replaceAll('ritual/hechizos.html','?nombre='+encodeURI(nombreHechizo));
	navigator.clipboard.writeText(url);
}

function setHechizo(hechizo){
	$('#texto_nombre').text(hechizo.nombre);
	$('#listaNombres').val(hechizo.nombre);
	$("#formulario input[name=id]").val(hechizo.id);
	$("#selClase").val(hechizo.clase).change();
	$("#formulario input[name=coste]").val(hechizo.coste).change();
	$("#formulario textarea[name=requisito]").val(hechizo.requisito).keyup();
	$("#formulario input[name=duracion]").val(hechizo.duracion).keyup();
	$("#formulario input[name=level]").val(hechizo.lvl).change();
	$("#formulario select[name=objetivo]").val(hechizo.objetivo).change();
	$("#formulario select[name=tipo]").val(hechizo.tipo).change();
	$("#formulario select[name=escuela]").val(hechizo.escuela).change();
	$("#formulario input[name=num_tipo]").val(hechizo.numTipo).change();
	$("#formulario input[name=alcance]").val(hechizo.alcance).change();
	$("#formulario textarea[name=efecto]").val(hechizo.efecto).keyup();
	$("#selTalento").val(hechizo.talento).change();
}

function loadSpellData(name){
	if (name===null) {
		return;
	}
	SPELLS_CONNECTION_MANAGER.findSpellDataByName(name,function(data){
			if(data!==null){
				if(!data.includes(';;')){
					setHechizo(JSON.parse(data)[0]);
				}
				else{
					notificacion('ERROR;;Algo ha salido mal, prueba otra vez.');
				}
			}
		}
 	);
}

function setupInputListeners(){
	$(".texto_nombre, textarea, input[name=duracion]").keyup(cambiaTexto);
	$("input[type=number]").change(cambiaTexto);			
	$("select[name=objetivo], select[name=tipo], select[name=escuela]").change(function(){
		let texto = $(this).find('option:selected').text();
		$('#texto_'+$(this).attr('name')).text(texto);			
	});
}

function setupButtons(){
	$("#agregar").click(function(){
		let hechizo = getSpell();
		if (!validateSpell(hechizo)) {
			return;
		}

		SPELLS_CONNECTION_MANAGER.insertSpell(hechizo, function(data){
			if(data.includes(';;')){
				notificacion(data);
			}else{
				notificacion('ERROR;;Ops! parece que algo ha salido mal 😵‍💫',true);
				console.error(data);
			}
		});
	});

	$("#modificar").click(function(){
		let updatedSpell = getSpell();
		if (!validateSpell(updatedSpell)) {
			return;
		}

		SPELLS_CONNECTION_MANAGER.updateSpell(updatedSpell, function(data){
			if(data.includes(';;')){
				notificacion(data);
				copiaEnlaceCompartirHechizo(updatedSpell.nombre);
				notificacion('OK;;Enlace copiado en el portapapeles 📄',true);
			}else{
				notificacion('ERROR;;Ops! parece que algo ha salido mal 😵‍💫',true);
				console.error(data);
			}
		});
	});
}