function muestraLoader(funcionLoad=function(){}){
	document.getElementById("loader").style.display = "block";
	funcionLoad();
}

function ocultaLoader(funcionOculta=function(){}){
	$("#loader").fadeOut("slow",function(){
		document.getElementById("loader").style.display = "none";
		funcionOculta();
	})
}