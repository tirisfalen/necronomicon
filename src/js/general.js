function notificacion(mensajeBruto,mensajeComoTitulo=false){
	let mensaje = mensajeBruto.split(";;");
	let info = mensaje[1];
	let titulo;
	let tipo;
	let sendNotification = true;

	switch (mensaje[0]) {
	  case "OK":
	    titulo = "Modificado con exito";
	    tipo = "success";
	    break;
	  case "ERROR":
	    titulo = "Error";
	    tipo = "error";
	    break;
	  case "INFO":
	    titulo = "Info";
	    tipo = "info";
	    break;
	  default:
			console.error("Notificacion de tipo invalido: "+mensaje[0]);
			console.log(mensaje);
	    break;
	}

	if (mensajeComoTitulo) {
		titulo=info;
		info="";
	}
	
	if(sendNotification){
		notification(titulo,info,tipo,2000);
	}
}

function getUrlParams(){
	return new URLSearchParams(window.location.search);
}

function validateUrlParams(urlParams){

	if (urlParams===undefined || urlParams===null) return false;

	return (urlParams.toString().length>0) ? true : false;
}

function setUrlParamsValue(urlParams, attrName, value){
	if(value==="" || value===null){
		urlParams.delete(attrName);
	}else{
		urlParams.set(attrName, value);
	}
}

function updateLocation(urlParams){
	let path = window.location.pathname;
	let queryParams = urlParamsToString(urlParams);
	history.pushState({}, '', `${path}${queryParams}`);
}

function urlParamsToString(urlParams){
	
	if (urlParams===undefined || urlParams===null) {
		return "";
	}

	const GET_CHARACTER = '?';

	if(validateUrlParams(urlParams)){
		return GET_CHARACTER+urlParams.toString();
	}

	return "";
}