<?

	// print_r($_POST); 

	if (isset($_GET["operacion"])) {
		$_POST["operacion"]=$_GET["operacion"];
	}

	if (isset($_POST["operacion"])) {
		switch ($_POST["operacion"]) {
			case "insertaHechizo()":
				include 'daos/HechizosDao.php';
				if (!isset($_POST['hechizo'])) {
					echo "ERROR;;No no hay hechizo";
					break;
				}
				$hechizo = new Hechizo(
						$_POST['hechizo']['id'],
						$_POST['hechizo']['clase'],
						$_POST['hechizo']['talento'],
						$_POST['hechizo']['nombre'],
						$_POST['hechizo']['coste'],
						$_POST['hechizo']['requisito'],
						$_POST['hechizo']['nivel'],
						$_POST['hechizo']['objetivo'],
						$_POST['hechizo']['tipo'],
						$_POST['hechizo']['numTipo'],
						$_POST['hechizo']['alcance'],
						$_POST['hechizo']['efecto'],
						null,
						$_POST['hechizo']['legendario'],
						$_POST['hechizo']['escuela'],
						$_POST['hechizo']['duracion']);
				echo HechizosDao::insertaHechizo($hechizo);
		        break;		
			case "getAllTiposObjetivos()":
				include 'daos/HechizosDao.php';
				echo json_encode(HechizosDao::getAllTiposObjetivos());
		        break;		
			case "getAllTiposHechizos()":
				include 'daos/HechizosDao.php';
				echo json_encode(HechizosDao::getAllTiposHechizos());
		        break;   
			case "getAllTiposEscuelas()":
				include 'daos/HechizosDao.php';
				echo json_encode(HechizosDao::getAllTiposEscuelas());
		        break;   
			case "getAllHechizosFromVista()":
				include 'daos/HechizosDao.php';
				echo json_encode(HechizosDao::getAllHechizosFromVista());
		        break;  
			case "getAllHechizos()":
				include 'daos/HechizosDao.php';
				echo json_encode(HechizosDao::getAllHechizos());
		        break;  
			case "findHechizosFromVistaByFilter()":
				include 'daos/HechizosDao.php';
				$filtro=(isset($_POST['filtro']))?Filtro::getInstanceOf($_POST['filtro']):null;
				$hechizos = ($filtro!=null)?HechizosDao::findHechizosFromVistaByFilter($filtro):null;
				echo json_encode($hechizos);
		        break;
			case "findHechizoByNombre()":
				include 'daos/HechizosDao.php';
				$filtro=Filtro::getInstanceOf(null);
				$filtro->setNombre($_POST['nombre']);
				$filtro->setLimit(1);
				$hechizo = HechizosDao::findHechizosByFilter($filtro);
				echo ($hechizo!=null)?json_encode($hechizo):"INFO;;No hay resultados";
		        break;
			case "modificaHechizo()":
				include 'daos/HechizosDao.php';
				$hechizo = new Hechizo(
						$_POST['hechizo']['id'],
						$_POST['hechizo']['clase'],
						$_POST['hechizo']['talento'],
						$_POST['hechizo']['nombre'],
						$_POST['hechizo']['coste'],
						$_POST['hechizo']['requisito'],
						$_POST['hechizo']['nivel'],
						$_POST['hechizo']['objetivo'],
						$_POST['hechizo']['tipo'],
						$_POST['hechizo']['numTipo'],
						$_POST['hechizo']['alcance'],
						$_POST['hechizo']['efecto'],
						null,
						$_POST['hechizo']['legendario'],
						$_POST['hechizo']['escuela'],
						$_POST['hechizo']['duracion']);
				echo HechizosDao::modificaHechizo($hechizo);
		        break;
			case "ping()":
				echo "pong";
		        break;
		     default:
		     	echo "ERROR;;La operacion ".$_POST["operacion"]." no existe";
		}
	}
?>