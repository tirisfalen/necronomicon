<?
include 'ClasesDao.php';
include 'TalentosDao.php';

/**
 * Intermediario de ClasesDao y TalentosDao con Manager y se encarga de la logica para organizar objetos.
 */
class ClasesTalentosDao
{

	public static function findClaseByNombre($nombre){
        $clase = ClasesDao::findClaseByNombre($nombre);
		$clase->talentos= TalentosDao::findTalentosByIdClase($clase->id);
		return $clase;
	}	

	public static function getAllClases(){
		include 'conecta.php';
        $clases = ClasesDao::getAllClases();
        $talentos = TalentosDao::getAllTalentos();

        for ($i=0; $i < count($clases); $i++) { 
        	$idClase = $clases[$i]->id;
        	for ($j=0; $j < count($talentos) ; $j++) { 
        		if ($idClase==$talentos[$j]->idClase)
			        $clases[$i]->addTalento($talentos[$j]);
        	}
        }
    	return $clases;
	}

	public static function getAllTalentos(){
		include 'conecta.php';
    	return TalentosDao::getAllTalentos();
	}

	// **************** QUERYS ********************

	public static function insertaClase($clase){
		if(!ClasesDao::insertaClase($clase))
			return "ERROR;;Error al insertar la clase";

		if(!$clase->hasTalentos())
			return "OK;;Clase insertada correctamente sin talentos";

		if(TalentosDao::insertaTalentos(ClasesDao::getLastId(),$clase->talentos))
			return "OK;;Clase insertada correctamente con talentos";
		else
			return "ERROR;;Error al insertar los talentos de la clase";
	}

	public static function updateClase($clase){
		if(!ClasesDao::updateClase($clase))
			return "ERROR;;Error al modificar la clase";

		if(!$clase->hasTalentos())
			return "OK;;Clase modificada correctamente sin talentos";
		
		$talentos=$clase->talentosListToUpdate();
		if (!$talentos==null)
			if(!TalentosDao::updateTalentos($clase->id,$talentos))
				return "ERROR;;Error actualizando los talentos";
		
		$talentos=$clase->talentosListToDelete();
		if (!$talentos==null)
			if(!TalentosDao::deleteTalentos($clase->id,$talentos))
				return "ERROR;;Error eliminando los talentos";

		$talentos=$clase->talentosListToInsert();
		if (!$talentos==null)
			if(!TalentosDao::insertaTalentos($clase->id,$talentos))
				return "ERROR;;Error al insertar los talentos";

		return "OK;;Clase modificada correctamente con talentos";
	}
}
?>