<?
/**
 * VO de una clase
 */
class Talento
{
	//VARIABLES
	public $id;
	public $idClase;
	public $nombre;
	public $color;
	public $hechizos;//arrObjetos

	public function __construct($id, $idClase, $nombre, $color, $hechizos)
	{
		$this->id=$id;
		$this->idClase = $idClase;
		$this->nombre=$nombre;
		$this->color = $color[0] === '#' ? $color : '#' . $color;
		$this->hechizos=$hechizos;
	}

	public static function parseTalentoFromDB($arr){
		return new Talento($arr['ID'],$arr['ID_CLASE'],$arr['NOMBRE'],$arr['COLOR'],null);
	}

	public static function createArrayTalentos($idsTalentos, $idClase, $nombresTalentos, $coloresTalentos, $hechizos){
    	$talentos;
    	for ($i=0; $i <count($nombresTalentos) ; $i++) { 
    		$talentos[$i] = new Talento($idsTalentos[$i], $idClase,$nombresTalentos[$i],$coloresTalentos[$i],$hechizos);
    	}
    	return $talentos;
	}

	public function getColor($hex=false){
		if($hex==true){
			return $this->color;
		}
		return substr($this->color, 1);
	}

	public function toInsert(){
		return ($this->id==0);
	}

	public function toDelete(){
		return ($this->nombre=="" && $this->color=="");
	}

	public function __toString(){
        return $this->nombre;
    }
}
?>