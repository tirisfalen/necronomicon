<?

/**
 * VO de un tipo
 */
class Tipo
{
	//VARIABLES
	public $id;
	public $nombre;

	public function __construct($id, $nombre)
	{
		$this->id=$id;
		$this->nombre=$nombre;
	}

	public static function parseTipoFromDB($arr){
		return new Tipo($arr['ID'],$arr['NOMBRE']);
	}	

	public static function parseTipoArrayFromDB($arr){
		$tipos = array('');
    	for ($i=0; $i <count($arr); $i++) { 
    		$tipos[$i] = Self::parseTipoFromDB($arr[$i]);
    	}
    	return $tipos;
	}

	public function __toString(){
		return $this->nombre;
    }
}
?>