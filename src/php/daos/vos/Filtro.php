<?
/**
 * VO que se usa para filtrar en la BD
 */
class Filtro
{
    //VARIABLES
    private $filter;
    private $nuevo;
    private $limit;
    private $hasWhere=false;

    public function __construct($nombre, $clase, $talento, $nuevo)
    {
        $this->filter['NOMBRE']=$nombre;
        $this->filter['CLASE']=$clase;
        $this->filter['TALENTO']=$talento;

        if($nombre!=null || $clase!=null || $talento!=null)
            $this->hasWhere=true;

        $this->nuevo=$nuevo;
        $this->limit=null;     
    }
    
    public static function getInstanceOf($arr){
        if($arr==null)
            return new Filtro(null,null,null,null); 

        return new Filtro( 
            (isset($arr['nombre']) && $arr['nombre']!="")?$arr['nombre']:null,
            (isset($arr['clase']) && $arr['clase']!="")?$arr['clase']:null,
            (isset($arr['escuela']) && $arr['escuela']!="")?$arr['escuela']:null,
            (isset($arr['nuevo']) && $arr['nuevo']!="")?true:false
        );
    }

    public function __toString(){
        return self::getQuery();
    }

    public function setNombre($nombre){
        $this->filter['NOMBRE']=$nombre;
        $this->hasWhere=true;
    }

    public function setLimit($limite){
        $this->limit=$limite;
    }

    public function getNuevo(){
        return $this->nuevo;
    }

    public function getQuery(){
        if (!$this->hasWhere) {
            return "";
        }

        $where = 'WHERE';
        $and=false;

        foreach ($this->filter as $campo => $valor) {
            if ($valor!=null) {
                
                if ($campo=="NOMBRE")
                    $campoValor = $campo." LIKE '%".$valor."%'";
                else{
                    if(!is_numeric($valor))
                        $valor='"'.$valor.'"';
                    $campoValor=$campo."=".$valor;
                }
                
                if($and==true){
                    $where=$where." AND ".$campoValor;
                }
                else{
                    $where=$where." ".$campoValor;
                    $and=true;
                }
            }
        }

        return $where;
    }

    public function getLimit(){
        return ($this->limit==null)?"":"LIMIT ".$this->limit;
    }
}
?>