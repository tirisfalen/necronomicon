<?
/**
 * Ojo que no tiene acceso a vos/Talentos a menos que se le indique
 */
class TalentosDao
{
	// **************** SELECT ********************

	public static function findTalentosByIdClase($idClase){
		include 'conecta.php';
		$sql="SELECT * FROM ".$T_TALENTOS." WHERE `ID_CLASE`=".$idClase;
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		$num_rows=count($datos);
		$talentos = array('');
    	for ($i=0; $i <$num_rows ; $i++) { 
    		$talentos[$i] = Talento::parseTalentoFromDB($datos[$i]);
    	}
    	return $talentos;
	}

	public static function getAllTalentos(){
		include 'conecta.php';
		$sql="SELECT * FROM ".$T_TALENTOS." ORDER BY NOMBRE";
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		$num_rows=count($datos);
		$talentos = array('');
    	for ($i=0; $i <$num_rows ; $i++) { 
    		$talentos[$i] = Talento::parseTalentoFromDB($datos[$i]);
    	}
    	return $talentos;
	}

	// **************** QUERYS ********************

	static function insertaTalentos($idClase, $talentos){
		include 'conecta.php';
		$sql = "INSERT INTO ".$T_TALENTOS." (ID_CLASE, COLOR, NOMBRE) VALUES ";
		for ($i=0; $i < count($talentos); $i++) { 
			$sql = $sql."('".$idClase."','".$talentos[$i]->getColor(false)."', '".$talentos[$i]->nombre."'),";
		}
		$sql=substr($sql, 0, -1);
		$mysqli->query($sql);
		return true;
	}

	static function updateTalentos($idClase, $talentos){
		include 'conecta.php';
		for ($i=0; $i < count($talentos); $i++) { 
			$sql = "UPDATE ".$T_TALENTOS." SET COLOR = '".$talentos[$i]->getColor(false)."', NOMBRE = '".$talentos[$i]->nombre."' WHERE ID = ".$talentos[$i]->id.";";
			$mysqli->query($sql);
		}
		return true;
	}

	static function deleteTalentos($idClase, $talentos){
		include 'conecta.php';
		for ($i=0; $i < count($talentos); $i++) { 
			$sql = "DELETE FROM ".$T_TALENTOS." WHERE ID = ".$talentos[$i]->id.";";
			$mysqli->query($sql);
		}
		return true;
	}
}
?>