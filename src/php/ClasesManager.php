<?

	// print_r($_POST); 

	if (isset($_GET["operacion"])) {
		$_POST["operacion"]=$_GET["operacion"];
	}

	if (isset($_POST["operacion"])) {

		switch ($_POST["operacion"]) {
		    case "insertaClase()":
				include 'daos/ClasesTalentosDao.php';
				$talentos = null;
				if (isset($_POST["clase"]["idsTalentos"])) {
					$talentos = Talento::createArrayTalentos($_POST["clase"]["idsTalentos"],
															 null,
												 			 $_POST["clase"]["nombreTalentos"],
												 			 $_POST["clase"]["colorTalentos"],
												 			 null);
				}
				$clase = new Clase(0,$_POST["clase"]["nombre"],$_POST["clase"]["color"],$talentos);
		        echo ClasesTalentosDao::insertaClase($clase);
		        break;		    
			case "modificaClase()":
				include 'daos/ClasesTalentosDao.php';
				$talentos = null;
				if (isset($_POST["clase"]["idsTalentos"])) {
					$talentos = Talento::createArrayTalentos($_POST["clase"]["idsTalentos"],
															 $_POST["clase"]["id"],
												 			 $_POST["clase"]["nombreTalentos"],
												 			 $_POST["clase"]["colorTalentos"],
												 			 null);
				}
				$clase = new Clase($_POST["clase"]["id"],$_POST["clase"]["nombre"],
									$_POST["clase"]["color"],$talentos);
		        echo ClasesTalentosDao::updateClase($clase);
		        break;
		    case "findClaseByNombre()":
				include 'daos/ClasesTalentosDao.php';
		        $clase = ClasesTalentosDao::findClaseByNombre($_POST["nombre"]);
		        echo json_encode($clase);
		        break;
		    case "getAllClases()":
				include 'daos/ClasesTalentosDao.php';
		        echo json_encode(ClasesTalentosDao::getAllClases());
		        break;
		    case "getAllTalentos()":
				include 'daos/ClasesTalentosDao.php';
		        echo json_encode(ClasesTalentosDao::getAllTalentos());
		        break;
			case "ping()":
				echo "pong";
				break;
		     default:
		     	echo "ERROR;;La operacion ".$_POST["operacion"]." no existe";
		}
	}
?>