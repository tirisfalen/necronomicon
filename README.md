
# Necronomicon

Este proyecto es una aplicación web que se utiliza para almacenar y consultar los hechizos de un manual de rol llamado Viejo Azeroth. La aplicación está diseñada para ser intuitiva y fácil de usar, con una interfaz amigable que permite a los usuarios insertar, modificar y consultar los hechizos de manera rápida y eficiente. La estructura del proyecto está organizada de manera clara y ordenada, permitiendo una fácil personalización y mantenimiento.

La aplicación está desarrollada con tecnologías web estándar, como HTML, CSS y JavaScript, y utiliza una base de datos relacional para almacenar los hechizos. La conexión entre la aplicación y la base de datos se realiza mediante peticiones HTTP y PHP.

## Estructura

    ├───src
    │   ├───css
    │   ├───img
    │   │   ├───ico
    │   │   └───clases
    │   └───js
    │   	├───*.js
    │       ├───notificaciones
    │       ├───php
    │  		|	├───*Manager.php
    │       │   ├───daos
    │       │   │   └───vos
    │       │   └───html
    │       └───loader
    ├───ritual
    │   ├───clases.html
    │   └───hechizos.html
    └───sql
    │   ├───datos.sql
    │   └───tables.sql
    └───index.html


### Managers.php
Los archivos manager son clases encargada de manejar las diferentes operaciones que se pueden realizar en el proyecto.

Estos archivos contienen diferentes casos o métodos los cuales son llamados mediante peticiones HTTP (GET o POST) desde la página web hacia el archivo manager.php, cada uno de estos casos se encarga de realizar una tarea específica como puede ser insertar un hechizo, obtener todos los tipos de hechizos, buscar hechizos por filtro, entre otros.

Cada uno de estos casos, utiliza una clase DAO para realizar las operaciones necesarias en la base de datos y devolver el resultado de esta operación al cliente. Este archivo manager se encarga de recibir los datos del cliente, procesarlos y enviarlo a la clase DAO correspondiente.

### Daos
Estos archivos DAO (Data Access Object) se utilizan para acceder a la base de datos y realizar las operaciones necesarias. En este caso, la clase se llama "HechizosDao" y se utiliza para acceder a la tabla de hechizos.

La clase tiene varios métodos estáticos que se utilizan para realizar diferentes operaciones con la base de datos, como obtener el último id insertado, obtener todos los tipos de hechizos, escuelas y objetivos, buscar hechizos con un filtro, obtener todos los hechizos, entre otras cosas.

Cada método realiza una consulta SQL utilizando una sentencia SELECT para obtener los datos de la base de datos, y luego utiliza los métodos parseHechizoArrayFromDB o parseHechizoArrayFromVista para convertir los datos

### VOs
Estos archivos VO (Value Object) se utilizan para representar un objeto de negocio. En este caso, la clase "Hechizo" se utiliza para representar un hechizo del juego de rol Viejo Azeroth.

La clase tiene varios atributos que se utilizan para almacenar la información de un hechizo, como el id, nombre, coste, requisito, nivel, objetivo, tipo, alcance, efecto, fecha de modificación, legendario, escuela y duración.

La clase también tiene varios métodos estáticos que se utilizan para convertir los datos de un array de la base de datos en un objeto Hechizo y viceversa. El método getInstanceOf es utilizado para obtener una instancia del objeto Hechizo a partir de un array.

## Instrucciones de uso

1.  Asegúrate de tener un servidor web y una BBDD configurados y en funcionamiento.
2.  Importa la estructura y los datos desde los archivos `tables.sql` y `datos.sql` en la carpeta sql.
5.  Accede a la página web desde tu navegador.

### Atención
Tener en cuenta que el proyecto esta en desarrollo y puede tener errores, se recomienda hacer pruebas antes de poner en producción. Si encuentra algún problema o tiene alguna sugerencia, por favor, no dude en contactarme.